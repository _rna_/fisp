package util

import (
	"github.com/spf13/viper"
)

type Config struct {
	DB                 string    `mapstructure:"DB"`
	DEFAULT_START_DATE string    `mapstructure:"DEFAULT_START_DATE"`
	SPLITWISE          Splitwise `mapstruture:"SPLITWISE"`
	FIREFLY            Firefly   `mapstruture:"FIREFLY"`
	USERS              []User    `mapstruture:"USERS"`
}

type Firefly struct {
	BASE_URL string `mapstructure:"BASE_URL"`
	API_KEY  string `mapstructure:"API_KEY"`
}

type Splitwise struct {
	API_KEY  string `mapstructure:"API_KEY"`
	GROUP_ID string `mapstructure:"GROUP_ID"`
}

type User struct {
	NAME         string `mapstructure:"NAME"`
	SPLITWISE_ID int64  `mapstructure:"SPLITWISE_ID"`
	FIREFLY_ID   string `mapstructure:"FIREFLY_ID"`
}

// LoadConfig reads configuration from file or environment variables.
func LoadConfig(path string) (config Config, err error) {
	viper.AddConfigPath(path)
	viper.SetConfigName("app")
	viper.SetConfigType("toml")

	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	err = viper.Unmarshal(&config)
	return
}
