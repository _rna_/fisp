package util

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"gitlab.com/_rna_/fisp/entity"
)

func HttpRequest(httpRequest entity.HttpRequest) (string, error) {
	client := http.Client{}
	url := httpRequest.Url

	// set path parameters
	for _, pathParam := range httpRequest.PathParameters {
		url += "/" + pathParam
	}

	// set query parameters
	for i, queryParam := range httpRequest.QueryParameters {
		if i == 0 {
			url += "?"
		} else {
			url += "&"
		}
		url += fmt.Sprintf("%s=%s", queryParam.Key, queryParam.Value)
	}

	log.Default().Printf("Sending request to %s\n", url)

	bodyString, err := json.Marshal(httpRequest.Body)
	if err != nil {
		return "", err
	}
	req, err := http.NewRequest(httpRequest.Method, url, bytes.NewBuffer(bodyString))
	if err != nil {
		return "", err
	}

	// set headers
	if httpRequest.Headers != nil {
		req.Header = httpRequest.Headers
	}
	if httpRequest.ContentType != "" {
		req.Header.Set("Content-Type", httpRequest.ContentType)
	}
	if httpRequest.Accept != "" {
		req.Header.Set("accept", httpRequest.ContentType)
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("user-agent", "fisp bridge")
	if httpRequest.Token != "" {
		req.Header.Set("Authorization", "Bearer "+httpRequest.Token)
	}

	response, err := client.Do(req)
	if err != nil {
		return "", err
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}

	responseString := string(responseData)

	if response.StatusCode >= http.StatusOK && response.StatusCode < 300 {
		return responseString, nil
	}

	return "", fmt.Errorf(responseString)
}
