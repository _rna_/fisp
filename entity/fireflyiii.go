package entity

type FireflyBudget struct {
	ID         string                  `json:"id"`
	Attributes FireflyBudgetAttributes `json:"attributes"`
}

type FireflyBudgetCreateRequest struct {
	Name             string  `json:"name"`
	AutoBudgetAmount float64 `json:"auto_budget_amount"`
}

type FireflyBudgetAttributes struct {
	Name string `json:"name"`
}

type FireflyBudgetCreateResponse struct {
	Data FireflyBudget `json:"data"`
}

type FireflyResponsePagination struct {
	CurrentPage int64 `json:"current_page"`
	TotalPages  int64 `json:"total_pages"`
}
type FireflyResponseMeta struct {
	Pagination FireflyResponsePagination `json:"pagination"`
}

type FireflyBudgetResponse struct {
	Data []FireflyBudget     `json:"data"`
	Meta FireflyResponseMeta `json:"meta"`
}
type FireflyTransactionCreateRequest struct {
	ErrorIfDuplicate bool                 `json:"error_if_duplicate_hash"`
	ApplyRules       bool                 `json:"apply_rules"`
	FireWebhooks     bool                 `json:"fire_webhooks"`
	GroupTitle       string               `json:"group_title"`
	Transactions     []FireflyTransaction `json:"transactions"`
}

type FireflyTransactionUpdateRequest struct {
	ErrorIfDuplicate bool                 `json:"error_if_duplicate_hash"`
	ApplyRules       bool                 `json:"apply_rules"`
	FireWebhooks     bool                 `json:"fire_webhooks"`
	GroupTitle       string               `json:"group_title"`
	Transactions     []FireflyTransaction `json:"transactions"`
}

type FireflyTransaction struct {
	Type          string `json:"type"`
	Description   string `json:"description"`
	Amount        string `json:"amount"`
	CurrencyCode  string `json:"currency_code"`
	BudgetID      string `json:"budget_id"`
	SourceID      string `json:"source_id,omitempty"`
	DestinationID string `json:"destination_id,omitempty"`
	Notes         string `json:"notes"`
	Date          string `json:"date"`
	PaymentDate   string `json:"payment_date"`
}

type FireflyTransactionSearchResponse struct {
	Data []FireflyTransactionSearch `json:"data"`
}

type FireflyTransactionSearch struct {
	Type string `json:"type"`
	ID   string `json:"id"`
	// rest not needed currently
}
