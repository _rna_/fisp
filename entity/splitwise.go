package entity

type SplitwiseExpensesResponse struct {
	Expenses []SplitwiseExpense `json:"expenses"`
}

type SplitwiseExpense struct {
	ID           int64                `json:"id"`
	Description  string               `json:"description"`
	Date         string               `json:"date"`
	Category     SplitwiseCategory    `json:"category"`
	Users        []SplitwiseGroupUser `json:"users"`
	CurrencyCode string               `json:"currency_code"`
	DeletedAt    string               `json:"deleted_at,omitempty"`
}

type SplitwiseCategory struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

type SplitwiseGroupUser struct {
	User      SplitwiseUser `json:"user"`
	UserID    int64         `json:"user_id"`
	PaidShare string        `json:"paid_share"`
	OwedShare string        `json:"owed_share"`
}

type SplitwiseUser struct {
	ID        int64  `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}
