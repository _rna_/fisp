package entity

type Expense struct {
	ID           int64
	Description  string
	Date         string
	Category     Category
	Users        []User
	CurrencyCode string
	Deleted      bool
}

type Category struct {
	ID   int64
	Name string
}

type User struct {
	Name          string
	SourceID      int64
	DestinationID string
	Amount        float64
}
