package entity

import "time"

type UpdateTime struct {
	ID   int64 `gorm:"primarykey"`
	Time time.Time
}

func NewUpdateTime(t time.Time) UpdateTime {
	return UpdateTime{Time: t}
}
