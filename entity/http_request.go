package entity

import "net/http"

type HttpRequest struct {
	Method          string
	Url             string
	QueryParameters []QueryParameter
	PathParameters  []string
	Body            interface{}
	Token           string
	ContentType     string
	Accept          string
	Headers         http.Header
}

type QueryParameter struct {
	Key   string
	Value string
}

func (h *HttpRequest) PutQuery(key string, value string) {
	h.QueryParameters = append(h.QueryParameters, QueryParameter{key, value})
}

func (h *HttpRequest) PutPath(value string) {
	h.PathParameters = append(h.PathParameters, value)
}
