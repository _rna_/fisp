# FiSp: A Splitwise - Firefly III Bridge

Acts as a bridge for syncing transactions from a Splitwise group to Firefly III, adding the corresponding budgets to the transactions. Splitwise is used for splitting the expenses between a group of people, there are several categories to add payments in Splitwise. But the list of payments grouped by the categories are not available in the Splitwise normal user. So, what this bridge does is it adds the transacations with the category to Firefly III which can display various graphs for different categories.

## Features 

This script works as a sync script which needs to be run at a regular interval since Splitwise does not support webhooks. The script will

* Add new transactions
* Update the existing transactions
* Delete the transactions

Moreover, the script needs a asset account in Firefly for each user in Splitwise. For each Splitwise transaction, there maybe multiple Firefly transactions using the asset accounts of different users. This allows to check if Splitwise and Firefly are in sync by checking the balances for each user.

FiSp only syncs the transactions of a single Splitwise group.

## Setting up

### Fill configuration

Copy the sample configuration file and fill in the values.
```sh
cp config/app.sample.toml config/app.toml
```
Now update the values of the file `config/app.toml`

### Add a cron job

(or a systemd file if you prefer)
Run time `main.go` crontab to update the transactions in ocassionally (use `crontab -e` to edit the crontab)
