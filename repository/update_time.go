package repository

import (
	"log"
	"time"

	"gitlab.com/_rna_/fisp/entity"
	"gorm.io/gorm"
)

type UpdateTime interface {
	Get() (entity.UpdateTime, error)
	Create(entity.UpdateTime) error
	Update(entity.UpdateTime) error
}

type updateTime struct {
	connection  *gorm.DB
	currentTime time.Time
}

func NewUpdateTime(connection *gorm.DB) UpdateTime {
	return &updateTime{
		connection,
		time.Now(),
	}
}

func (u *updateTime) Get() (entity.UpdateTime, error) {
	var updateTime entity.UpdateTime

	result := u.connection.Last(&updateTime)

	if result.Error != nil {
		return entity.UpdateTime{}, result.Error
	}

	return updateTime, nil
}

func (u *updateTime) Create(updateTime entity.UpdateTime) error {
	result := u.connection.Create(&updateTime)

	if result.Error != nil {
		log.Default().Println(result.Error)
		return result.Error
	}

	return nil
}

func (u *updateTime) Update(updateTime entity.UpdateTime) error {
	result := u.connection.Save(&updateTime)

	if result.Error != nil {
		return result.Error
	}

	return nil
}
