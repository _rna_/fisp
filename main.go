package main

import (
	"log"

	"gitlab.com/_rna_/fisp/db/sqlite"
	"gitlab.com/_rna_/fisp/entity"
	"gitlab.com/_rna_/fisp/repository"
	"gitlab.com/_rna_/fisp/service"
	"gitlab.com/_rna_/fisp/service/destination"
	"gitlab.com/_rna_/fisp/service/source"
	"gitlab.com/_rna_/fisp/util"
)

func main() {
	logger := log.Default()
	config, err := util.LoadConfig("config")
	if err != nil {
		logger.Fatal("cannot load config:", err)
	}

	db := sqlite.NewSqlite(config.DB)
	err = db.AutoMigrate(entity.UpdateTime{})
	if err != nil {
		logger.Fatal("cannot auto migrate database: ", err)
	}

	// repository initialize
	updateTimeRepository := repository.NewUpdateTime(db)

	// service initialize
	updateTimeService := service.NewUpdateTime(updateTimeRepository, config.DEFAULT_START_DATE)
	splitwiseSource := source.NewSplitwise(config.SPLITWISE.API_KEY, config.USERS)
	fireflyDestination := destination.NewFireflyIII(config.FIREFLY.BASE_URL, config.USERS, config.FIREFLY.API_KEY)

	lastUpdated := updateTimeService.GetLatest()
	logger.Println("Last updated on " + lastUpdated.Time.String())
	expenses, err := splitwiseSource.GetExpensesUpdatedAfter(config.SPLITWISE.GROUP_ID, lastUpdated)
	if err != nil {
		return
	}

	err = fireflyDestination.AddOrUpdateTransactions(expenses)
	if err != nil {
		logger.Fatal(err)
	}

	err = updateTimeService.UpdateLatest()
	if err != nil {
		logger.Println(err)
		return
	}

}
