module gitlab.com/_rna_/fisp

go 1.16

require (
	github.com/spf13/viper v1.9.0
	gorm.io/driver/sqlite v1.2.6 // indirect
	gorm.io/gorm v1.22.4 // indirect
)
