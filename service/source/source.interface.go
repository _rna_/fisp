package source

import (
	"gitlab.com/_rna_/fisp/entity"
)

type Source interface {
	GetExpensesUpdatedAfter(string, entity.UpdateTime) ([]entity.Expense, error)
}
