package source

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"time"

	"gitlab.com/_rna_/fisp/entity"
	"gitlab.com/_rna_/fisp/util"
)

type splitwise struct {
	baseUrl string
	token   string
	users   map[int64]util.User
}

func NewSplitwise(token string, usersConfig []util.User) Source {
	users := make(map[int64]util.User)
	for _, userConfig := range usersConfig {
		users[userConfig.SPLITWISE_ID] = userConfig
	}
	return &splitwise{
		"https://secure.splitwise.com/api/v3.0",
		token,
		users,
	}
}

func (s *splitwise) GetExpensesUpdatedAfter(groupId string, updatedAfter entity.UpdateTime) ([]entity.Expense, error) {

	// fetch the data
	limit := 10000
	httpRequest := entity.HttpRequest{
		Method: "GET",
		Url:    s.baseUrl,
		Token:  s.token,
	}

	httpRequest.PutPath("get_expenses")

	httpRequest.PutQuery("group_id", groupId)
	httpRequest.PutQuery("updated_after", s.convertToFormattedTime(updatedAfter.Time))
	httpRequest.PutQuery("limit", fmt.Sprint(limit))

	responseJson, err := util.HttpRequest(httpRequest)
	if err != nil {
		log.Default().Println("Couldn't fetch data from splitwise", err)
		return nil, err
	}

	// unmarshall the data
	var response entity.SplitwiseExpensesResponse
	err = json.Unmarshal([]byte(responseJson), &response)
	if err != nil {
		log.Default().Println("Unmarshelling splitwise response failed")
		return nil, err
	}

	var expenses []entity.Expense
	for _, expense := range response.Expenses {
		expenses = append(expenses, s.splitwiseExpenseToExpense(expense))
	}

	return expenses, nil
}

func (s *splitwise) convertToFormattedTime(time time.Time) string {
	layout := "2006-01-02T15:04:05Z07:00"
	return time.Format(layout)
}

func (s *splitwise) splitwiseExpenseToExpense(expense entity.SplitwiseExpense) entity.Expense {

	category := entity.Category{
		ID:   expense.Category.ID,
		Name: expense.Category.Name,
	}

	var users []entity.User
	for _, user := range expense.Users {
		userConfig := s.users[user.UserID]
		paidShare, err := strconv.ParseFloat(user.PaidShare, 64)
		if err != nil {
			log.Fatalf("Couldn't convert %s paid share to float\n", user.PaidShare)
		}
		owedShare, err := strconv.ParseFloat(user.OwedShare, 64)
		if err != nil {
			log.Fatalf("Couldn't convert %s paid share to float\n", user.OwedShare)
		}

		if paidShare != 0 {
			users = append(users, entity.User{
				Name:          userConfig.NAME,
				SourceID:      user.UserID,
				DestinationID: userConfig.FIREFLY_ID,
				Amount:        paidShare,
			})
		}

		if owedShare != 0 {
			users = append(users, entity.User{
				Name:          userConfig.NAME,
				SourceID:      user.UserID,
				DestinationID: userConfig.FIREFLY_ID,
				Amount:        -1 * owedShare,
			})
		}

	}

	deleted := false
	if expense.DeletedAt != "" {
		deleted = true
	}
	return entity.Expense{
		ID:           expense.ID,
		Description:  expense.Description,
		Date:         expense.Date,
		Category:     category,
		Users:        users,
		CurrencyCode: expense.CurrencyCode,
		Deleted:      deleted,
	}
}
