package destination

import (
	"gitlab.com/_rna_/fisp/entity"
)

type Destination interface {
	AddOrUpdateTransactions([]entity.Expense) error
}
