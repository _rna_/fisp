package destination

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"

	"gitlab.com/_rna_/fisp/entity"
	"gitlab.com/_rna_/fisp/util"
)

type fireflyiii struct {
	baseUrl string
	token   string
	users   []util.User
	budgets []entity.FireflyBudget
}

func NewFireflyIII(baseUrl string, users []util.User, token string) Destination {
	var budgets []entity.FireflyBudget

	return &fireflyiii{
		baseUrl,
		token,
		users,
		budgets,
	}
}

func (f *fireflyiii) AddOrUpdateTransactions(expenses []entity.Expense) error {
	// asset accounts are assumed to exist

	// list all the budgets available
	budgets, err := f.fetchAllBudgets()
	if err != nil {
		return err
	}
	f.budgets = budgets

	for _, expense := range expenses {
		// Find the budget id of the transaction
		budgetID, err := f.findOrCreateBudget(expense.Category)
		if err != nil {
			return err
		}
		err = f.AddTransaction(expense, budgetID)
		if err != nil {
			return err
		}
	}

	return nil
}

func (f *fireflyiii) findOrCreateBudget(category entity.Category) (string, error) {

	for _, budget := range f.budgets {
		if budget.Attributes.Name == category.Name {
			return budget.ID, nil
		}
	}

	log.Default().Printf("Budget '%s' not found, creating new budget\n", category.Name)

	budget, err := f.createBudget(category)
	if err != nil {
		return "", err
	}
	f.budgets = append(f.budgets, *budget)

	return budget.ID, nil
}

func (f *fireflyiii) createBudget(category entity.Category) (*entity.FireflyBudget, error) {

	var response entity.FireflyBudgetCreateResponse
	autoBudgetAmount := 1000.00

	body := entity.FireflyBudgetCreateRequest{
		Name:             category.Name,
		AutoBudgetAmount: autoBudgetAmount,
	}

	request := entity.HttpRequest{
		Method:      "POST",
		Url:         f.baseUrl,
		Body:        body,
		Token:       f.token,
		ContentType: "application/json",
		Accept:      "application/vnd.api+json",
	}
	request.PutPath("api/v1/budgets")

	responseJson, err := util.HttpRequest(request)
	if err != nil {
		log.Default().Fatalln(err)
		return nil, err
	}

	err = json.Unmarshal([]byte(responseJson), &response)
	if err != nil {
		log.Default().Println("Unmarshelling firefly budget response failed", err)
		return nil, err
	}

	return &response.Data, nil
}

func (f *fireflyiii) fetchAllBudgets() ([]entity.FireflyBudget, error) {

	var budgets []entity.FireflyBudget
	var page int64 = 1
	for {
		budgetPage, err := f.fetchBudgetsPage(page)
		if err != nil {
			return budgets, err
		}

		budgets = append(budgets, budgetPage.Data...)

		if budgetPage.Meta.Pagination.CurrentPage >= budgetPage.Meta.Pagination.TotalPages {
			break
		}
		page++
	}

	return budgets, nil
}

func (f *fireflyiii) fetchBudgetsPage(page int64) (entity.FireflyBudgetResponse, error) {
	var response entity.FireflyBudgetResponse

	request := entity.HttpRequest{
		Url:    f.baseUrl,
		Method: "GET",
		Token:  f.token,
	}
	request.PutPath("api/v1/budgets")
	request.PutQuery("page", strconv.FormatInt(page, 10))

	responseJson, err := util.HttpRequest(request)
	if err != nil {
		log.Default().Fatalln(err)
		return response, err
	}

	err = json.Unmarshal([]byte(responseJson), &response)
	if err != nil {
		log.Default().Println("Unmarshelling firefly budget response failed", err)
		return response, err
	}

	return response, nil
}

func (f *fireflyiii) searchTransaction(query string) (*[]entity.FireflyTransactionSearch, error) {
	var response entity.FireflyTransactionSearchResponse

	request := entity.HttpRequest{
		Url:    f.baseUrl,
		Method: "GET",
		Token:  f.token,
	}
	request.PutPath("api/v1/search/transactions")
	request.PutQuery("query", query)

	responseJson, err := util.HttpRequest(request)
	if err != nil {
		log.Default().Fatalln(err)
		return nil, err
	}

	err = json.Unmarshal([]byte(responseJson), &response)
	if err != nil {
		log.Default().Println("Unmarshelling firefly budget response failed", err)
		return nil, err
	}

	return &response.Data, nil
}

func (f *fireflyiii) createTransaction(transaction entity.FireflyTransactionCreateRequest) error {
	request := entity.HttpRequest{
		Url:         f.baseUrl,
		Method:      "POST",
		Token:       f.token,
		Body:        transaction,
		ContentType: "application/json",
		Accept:      "application/vnd.api+json",
	}
	request.PutPath("api/v1/transactions")

	_, err := util.HttpRequest(request)
	return err
}

func (f *fireflyiii) updateTransaction(transactionID string, transaction entity.FireflyTransactionUpdateRequest) error {
	request := entity.HttpRequest{
		Url:         f.baseUrl,
		Method:      "PUT",
		Token:       f.token,
		Body:        transaction,
		ContentType: "application/json",
		Accept:      "application/vnd.api+json",
	}
	request.PutPath("api/v1/transactions")
	request.PutPath(transactionID)

	_, err := util.HttpRequest(request)
	return err
}

func (f *fireflyiii) deleteTransaction(transactionID string) error {
	request := entity.HttpRequest{
		Url:    f.baseUrl,
		Method: "DELETE",
		Token:  f.token,
		Accept: "application/vnd.api+json",
	}
	request.PutPath("api/v1/transactions")
	request.PutPath(transactionID)

	_, err := util.HttpRequest(request)
	return err
}

func (f *fireflyiii) AddTransaction(expense entity.Expense, budgetID string) error {

	log.Default().Printf("Adding transaction '%s' with budget id '%s' \n", expense.Description, budgetID)

	// for each user store a seperate transaction
	for _, user := range expense.Users {

		if user.Amount == 0 {
			continue
		}

		// create the transaction
		transaction := entity.FireflyTransaction{
			Type:         "withdrawal",
			Description:  expense.Description,
			CurrencyCode: expense.CurrencyCode,
			Amount:       strconv.FormatFloat(user.Amount, 'f', 6, 64),
			BudgetID:     budgetID,
			SourceID:     user.DestinationID,
			Notes:        "Transaction created by FiSp",
			Date:         expense.Date,
			PaymentDate:  expense.Date,
		}

		if user.Amount < 0 {
			transaction.Amount = strconv.FormatFloat(-1*user.Amount, 'f', 6, 64)
			transaction.Type = "deposit"
			transaction.DestinationID = user.DestinationID
			transaction.SourceID = ""
			transaction.BudgetID = ""
		}

		// check if the transaction exists
		uniqueID := fmt.Sprintf("%d|%d|%s", expense.ID, user.SourceID, transaction.Type)
		results, err := f.searchTransaction(uniqueID)
		if err != nil {
			return err
		}

		if len(*results) > 0 {
			if len(*results) != 1 {
				log.Default().Println(expense)
				return fmt.Errorf("more than 1 transaction came with the same search")
			}

			result := (*results)[0]

			if expense.Deleted {
				log.Default().Printf("Deleting the transaction %s\n", result.ID)
				err := f.deleteTransaction(result.ID)
				if err != nil {
					return err
				}
				continue
			}

			log.Default().Println("Updating the transaction")
			fireflyTransactionUpdateRequest := entity.FireflyTransactionUpdateRequest{
				ErrorIfDuplicate: true,
				ApplyRules:       true,
				FireWebhooks:     true,
				GroupTitle:       uniqueID,
				Transactions:     []entity.FireflyTransaction{transaction},
			}

			err = f.updateTransaction(result.ID, fireflyTransactionUpdateRequest)
			if err != nil {
				return err
			}
			continue
		}

		if expense.Deleted {
			continue
		}

		// transaction does not exists, create transaction
		fireflyTransactionCreateRequest := entity.FireflyTransactionCreateRequest{
			ErrorIfDuplicate: true,
			ApplyRules:       true,
			FireWebhooks:     true,
			GroupTitle:       uniqueID,
			Transactions:     []entity.FireflyTransaction{transaction},
		}

		log.Default().Println("Creating the transaction")
		err = f.createTransaction(fireflyTransactionCreateRequest)
		if err != nil {
			return err
		}
	}

	return nil
}
