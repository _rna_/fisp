package service

import (
	"time"

	"gitlab.com/_rna_/fisp/entity"
	"gitlab.com/_rna_/fisp/repository"
)

type UpdateTime interface {
	GetLatest() entity.UpdateTime
	UpdateLatest() error
}

type updateTime struct {
	defaultStartDate string
	repository       repository.UpdateTime
	startTime        time.Time
}

func NewUpdateTime(repository repository.UpdateTime, defaultStartDate string) UpdateTime {
	return &updateTime{
		defaultStartDate,
		repository,
		time.Now(),
	}
}

func (u *updateTime) GetLatest() entity.UpdateTime {
	t, err := u.repository.Get()
	if err != nil {
		tempTime, _ := time.Parse("02-01-2006", u.defaultStartDate)
		t = entity.NewUpdateTime(tempTime)
	}
	return t
}

func (u *updateTime) UpdateLatest() error {
	startTime := entity.NewUpdateTime(u.startTime)
	return u.repository.Create(startTime)
}
